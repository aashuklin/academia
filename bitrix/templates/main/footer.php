<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<div class="main_container homepage">

    <!-- events -->
    <div class="ev_events">
        <div class="ev_h">
            <h3>Ближайшие события</h3>
            <a href="" class="ev_allevents">Все мероприятия &rarr;</a>
        </div>
        <ul class="ev_lastevent">
            <li>
                <h4><a href="">29 августа 2012, Москва</a></h4>
                <p>Семинар производителей мебели России и СНГ, Обсуждение тенденций.</p>
            </li>
            <li>
                <h4><a href="">30 августа 2012, Санкт-Петербург</a></h4>
                <p>Открытие шоу-рума на Невском проспекте. Последние модели в большом ассортименте.</p>
            </li>
            <li>
                <h4><a href="">31 августа 2012, Краснодар</a></h4>
                <p>Открытие нового магазина в нашей дилерской сети.</p>
            </li>
        </ul>
        <div class="clearboth"></div>
    </div>
    <!-- // end events -->
    <div class="cn_hp_content">
        <div class="cn_hp_category">
            <ul>
                <li>
                    <img src="/bitrix/templates/.default/content/1.png" alt=""/>
                    <h2><a href="">Мягкая мебель</a></h2>
                    <p>Диваны, кресла и прочая мягкая мебель <a class="cn_hp_categorymore" href="">&rarr;</a></p>
                    <div class="clearboth"></div>
                </li>
                <li>
                    <img src="/bitrix/templates/.default/content/2.png" alt=""/>
                    <h2><a href="">Офисная мебель</a></h2>
                    <p>Диваны, столы, стулья <a class="cn_hp_categorymore" href="">&rarr;</a></p>
                    <div class="clearboth"></div>
                </li>
                <li>
                    <img src="/bitrix/templates/.default/content/3.png" alt=""/>
                    <h2><a href="">Мебель для кухни</a></h2>
                    <p>Полки, ящики, столы и стулья <a class="cn_hp_categorymore" href="">&rarr;</a></p>
                    <div class="clearboth"></div>
                </li>
                <li>
                    <img src="/bitrix/templates/.default/content/4.png" alt=""/>
                    <h2><a href="">Детская мебель</a></h2>
                    <p>Кровати, стулья, мягкая детская мебель <a class="cn_hp_categorymore" href="">&rarr;</a></p>
                    <div class="clearboth"></div>
                </li>
            </ul>
            <a href="" class="cn_hp_category_more">Все разделы каталога &rarr;</a>
        </div>
        <div class="cn_hp_post">
            <div class="cn_hp_post_new">
                <h3>Новинки</h3>
                <img src="/bitrix/templates/.default/content/7.png" alt=""/>
                <p>Угловой диван "Титаник", с большим выбором расцветок и фактур.</p>
                <div class="clearboth"></div>
            </div>
            <div class="cn_hp_post_action">
                <h3>Акции</h3>
                <img src="/bitrix/templates/.default/content/7.png" alt=""/>
                <p>Угловой диван "Титаник", с большим выбором расцветок и фактур.</p>
                <div class="clearboth"></div>
            </div>
            <div class="cn_hp_post_bestsellersn">
                <h3>Хиты продаж</h3>
                <img src="/bitrix/templates/.default/content/7.png" alt=""/>
                <p>Угловой диван "Титаник", с большим выбором расцветок и фактур.</p>
                <div class="clearboth"></div>
            </div>
        </div>
        <div class="cn_hp_lastnews">
            <h3><a href="">Новости</a></h3>
            <ul>
                <li>
                    <h4><a href="">29 августа 2012</a></h4>
                    <p>Поступление лучших офисных кресел из Германии</p>
                </li>
                <li>
                    <h4><a href="">29 августа 2012</a></h4>
                    <p>Мастер-класс дизайнеров из Италии для производителей мебели</p>
                </li>
                <li>
                    <h4><a href="">29 августа 2012</a></h4>
                    <p>Открытие нашего нового офиса рядом с метро Измайлово</p>
                </li>
                <li>
                    <h4><a href="">29 августа 2012</a></h4>
                    <p>Наша дилерская сеть расширилась теперь ассортимент наших товаров доступен в Казани</p>
                </li>
            </ul>
            <br/>
            <a href="" class="cn_hp_lastnews_more">Все новости &rarr;</a>
        </div>
        <div class="clearboth"></div>
    </div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"reviews", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "reviews",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "reviews",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "position",
			1 => "company",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
    <?include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/.default/include/footer.php');?>
</div>
</body>
</html>
