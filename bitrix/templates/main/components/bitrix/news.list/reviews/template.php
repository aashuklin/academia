<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>


<?if($arResult["ITEMS"]):?>

    <script type="text/javascript" >
        $(document).ready(function(){

            $("#foo").carouFredSel({
                items:2,
                prev:'#rwprev',
                next:'#rwnext',
                scroll:{
                    items:1,
                    duration:2000
                }
            });
        });
    </script>

    <div class="rw_reviewed">
        <div class="rw_slider">
            <h4><?=GetMessage("MAIN_REVIEW")?></h4>
            <?if($arParams["DISPLAY_TOP_PAGER"]):?>
                <?=$arResult["NAV_STRING"]?><br />
            <?endif;?>
            <ul id="foo">
                <?foreach($arResult["ITEMS"] as $arItem):?>
                <li>
                    <div class="rw_message">
                        <?if(is_array($arItem["PREVIEW_PICTURE"])):?>
                            <img src=<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>  class="rw_avatar" alt=""/>
                        <?endif;?>

                        <?if($arItem["NAME"]):?>
                            <span class="rw_name"><?=$arItem["NAME"]?>:&nbsp;</span>
                        <?endif;?>
                        <?if($arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"] && $arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]):?>
                            <span class="rw_job"><?=$arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"]?> <?=$arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]?></span>
                        <?elseif ($arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"] ):?>
                            <span class="rw_job"><?=$arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"]?></span>
                        <?elseif ($arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]):?>
                            <span class="rw_job"><?=$arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]?></span>
                        <?endif;?>
                        <?if($arItem["PREVIEW_TEXT"]):?>
                            <p><?=$arItem["PREVIEW_TEXT"]?></p>
                        <?endif;?>
                        <div class="clearboth"></div>
                        <div class="rw_arrow"></div>
                    </div>
                </li>
                <?endforeach;?>
            </ul>
            <div id="rwprev"></div>
            <div id="rwnext"></div>
            <a href="/company/reviews/" class="rw_allreviewed"><?=GetMessage("MAIN_ALL_REVIEW")?></a>
        </div>
    </div>
<?endif;?>

