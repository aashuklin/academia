<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if($arResult):?>
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?if($arItem["NAME"] && $arItem["DETAIL_PAGE_URL"]):?>
            <div class="ps_head">
                <a class="ps_head_link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <h2 class="ps_head_h"><?=$arItem["NAME"]?></h2>
                </a>
            <?if ($arItem["DISPLAY_ACTIVE_FROM"]):?>
                <span class="ps_date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                </div>
            <?endif;?>
        <?endif;?>
        <?if(is_array($arItem["PREVIEW_PICTURE"]) || $arItem["PREVIEW_TEXT"]):?>
            <div class="ps_content">
            <?if(is_array($arItem["PREVIEW_PICTURE"])):?>
                <img src=<?=$arItem["PREVIEW_PICTURE"]["SRC"]?> align="left" alt="<?=$arItem["NAME"]?>"/>
            <?endif;?>
            <?if($arItem["PREVIEW_TEXT"]):?>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
            <?endif;?>
            </div>
        <?endif;?>
        <div style="clear: both"></div>
    <?endforeach;?>
<?endif;?>

