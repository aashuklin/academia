<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if($arResult["NAME"]):?>
    <div class="main_title">
        <p class="title"><?=$arResult["NAME"]?></p>
        <?if($arResult["DISPLAY_ACTIVE_FROM"]):?>
            <span class="main_date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
        <?endif;?>
    </div>
<?endif;?>
<?if($arResult["DETAIL_TEXT"] || is_array($arResult["DETAIL_PICTURE"])):?>
    <div class="ps_content">
        <?if(is_array($arResult["DETAIL_PICTURE"])):?>
            <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" align="left" alt="<?=$arResult["NAME"]?>">
        <?endif;?>
        <?if($arResult["DETAIL_TEXT"]):?>
            <p><?=$arResult["DETAIL_TEXT"]?></p>
        <?endif;?>
    </div>
<?endif;?>

