<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


$this->setFrameMode(true);
?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<?if($arResult["ITEMS"]):?>
    <?$arItem = $arResult["ITEMS"][0]?>
    <div class="sb_reviewed">
        <?if(is_array($arItem["PREVIEW_PICTURE"])):?>
            <img src= <?=$arItem["PREVIEW_PICTURE"]["SRC"]?> class="sb_rw_avatar" alt=""/>
        <?endif;?>
        <?if($arItem["NAME"]):?>
            <span class="sb_rw_name"><?=$arItem["NAME"]?></span>
        <?endif;?>

        <?if($arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"] && $arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]):?>
            <span class="sb_rw_job"><?=$arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"]?> <?=$arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]?></span>
        <?elseif ($arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"]):?>
            <span class="sb_rw_job"><?=$arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"]?></span>
        <?elseif ($arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]):?>
            <span class="sb_rw_job"><?=$arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]?></span>
        <?endif;?>

        <?if($arItem["PREVIEW_TEXT"]):?>
            <p><?=$arItem["PREVIEW_TEXT"]?></p>
        <?endif;?>

        <div class="clearboth"></div>
        <div class="sb_rw_arrow"></div>
    </div>
<?endif;?>