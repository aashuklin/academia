<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?><br />
<?endif;?>

<?if($arResult["ITEMS"]):?>
    <?$arItem = $arResult["ITEMS"][0];?>
    <div class="sb_action">
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src=<?=$arItem["DETAIL_PICTURE"]["SRC"]?> alt=""/></a>
        <h4><?=GetMessage('SALE')?> </h4>

        <h5><a href=""><?=$arItem["NAME"]." ".GetMessage('ONLY_FOR')." ".$arItem["DISPLAY_PROPERTIES"]["price"]["VALUE"];?> Р</a></h5>
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="sb_action_more"><?=GetMessage('READ_MORE')?> &rarr;</a>
    </div>
<?endif;?>