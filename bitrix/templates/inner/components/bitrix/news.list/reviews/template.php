<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


$this->setFrameMode(true);
?>
<?if($arResult["ITEMS"]):?>
    <div class="main_post">
            <?if($arParams["DISPLAY_TOP_PAGER"]):?>
                <?=$arResult["NAV_STRING"]?><br />
            <?endif;?>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="review-block" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="review-text">
                    <div class="review-block-title">
                        <?if($arItem["NAME"]):?>
                            <span class="review-block-name"><a href="#"><?=$arItem["NAME"]?></a></span>
                        <?endif;?>
                        <?if($arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"] && $arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]):?>
                            <span class="review-block-description"><?=$arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"]?> <?=$arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]?></span></div>
                        <?elseif ($arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"]):?>
                            <span class="review-block-description"><?=$arItem["DISPLAY_PROPERTIES"]["position"]["VALUE"]?></span></div>
                        <?elseif ($arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]):?>
                            <span class="review-block-description"><?$arItem["DISPLAY_PROPERTIES"]["company"]["VALUE"]?></span></div>
                        <?endif;?>
                    <div class="review-text-cont">
                        <?=$arItem["PREVIEW_TEXT"]?>
                    </div>
                <?if (is_array($arItem["PREVIEW_PICTURE"])):?>
                    <div class="review-img-wrap">
                        <a href="#"><img src=<?=$arItem["PREVIEW_PICTURE"]["SRC"]?> alt="img"></a>
                    </div>
                <?endif;?>
            </div>
           </div>
        <?endforeach;?>
        <div class="clearboth"></div>
    </div>
<?endif;?>

